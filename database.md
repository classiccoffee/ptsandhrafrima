#### Database PT San Dhra Frima

1. STNK:

   ```bash
   * no_plat   
   * nama_pemilik   
   * alamat   
   * merk   
   * type   
   * jenis   
   * model   
   * tahun_pembuatan   
   * isi_silinder   
   * no_rangka   
   * no_mesin   
   * warna   
   * bahan_bakar   
   * warna_plat   
   * tahun_registrasi   
   * no_bpkb   
   * no_lokasi   
   * no_urut_daftar   
   * masa_berlaku   
   * status_stnk   
   ```
   ```bash
    DROP TABLE IF EXISTS `stnk`;
    CREATE TABLE `stnk` (
      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
      `no_plat` varchar(50) NOT NULL DEFAULT '',
      `nama_pemilik` varchar(50) NOT NULL DEFAULT '',
      `alamat` varchar(50) NOT NULL DEFAULT '',
      `merk` varchar(50) NOT NULL DEFAULT '',
      `type` varchar(50) NOT NULL DEFAULT '',
      `jenis` varchar(50) NOT NULL DEFAULT '',
      `model` varchar(50) NOT NULL DEFAULT '',
      `tahun_pembuatan` varchar(50) NOT NULL DEFAULT '',
      `isi_silinder` varchar(50) NOT NULL DEFAULT '',
      `no_rangka` varchar(50) NOT NULL DEFAULT '',
      `no_mesin` varchar(50) NOT NULL DEFAULT '',
      `warna` varchar(50) NOT NULL DEFAULT '',
      `bahan_bakar` varchar(50) NOT NULL DEFAULT '',
      `warna_plat` varchar(50) NOT NULL DEFAULT '',
      `tahun_registrasi` varchar(50) NOT NULL DEFAULT '',
      `no_bpkb` varchar(50) NOT NULL DEFAULT '',
      `no_lokasi` varchar(50) NOT NULL DEFAULT '',
      `no_urut_daftar` varchar(50) NOT NULL DEFAULT '',
      `masa_berlaku` varchar(50) NOT NULL DEFAULT '',
      `status_stnk` varchar(50) NOT NULL DEFAULT '',
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   #  DROP TABLE IF EXISTS `stnk`;
   #  CREATE TABLE IF NOT EXISTS `stnk` (
   #  `id` int(11) NOT NULL,
   #    `key` varchar(40) NOT NULL,
   #    `level` int(2) NOT NULL,
   #    `ignore_limits` int(1) NOT NULL DEFAULT '0',
   #    `is_private_key` int(1) NOT NULL DEFAULT '0',
   #    `ip_addresses` text,
   #    `date_created` int(11) NOT NULL
   #  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
   ```
   > Pengurusan Yang Berkaitan STNK.
   >
   > Tablenya ada :

   1. Pengurusan Pajak
   2. Pengurusan Hilang
   3. Pengurusan Perpanjangan
   4. BBN

     ```bash
     * no_plat/no_polisi
     * kategori_urusan
     * nama_urusan
     * biaya
     * tgl_urus
     * tgl_selesai
     * tgl_create_data
     * tgl_edit_data
     * status_urusan
     ```
     ```bash
      DROP TABLE IF EXISTS `urusan_adm_stnk`;
      CREATE TABLE `urusan_adm_stnk` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `no_plat` varchar(50) NOT NULL DEFAULT '',
        `kategori_urusan` varchar(50) NOT NULL DEFAULT '',
        `nama_urusan` varchar(50) NOT NULL DEFAULT '',
        `nama_pengurus` varchar(50) NOT NULL DEFAULT '',
        `no_ktp_pengurus` varchar(50) NOT NULL DEFAULT '',
        `biaya` varchar(50) NOT NULL DEFAULT '',
        `tgl_urusan` varchar(50) NOT NULL DEFAULT '',
        `tgl_selesai` varchar(50) NOT NULL DEFAULT '',
        `create_date` varchar(50) NOT NULL DEFAULT '',
        `update_date` varchar(50) NOT NULL DEFAULT '',
        `status_urusan` varchar(50) NOT NULL DEFAULT '',
        `ketreangan` varchar(50) NOT NULL DEFAULT '',
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      ```
2. Daftar Pemilik

   ```bash
   * no_ktp
   * nama
   * no_plat/no_polisi
   * alamat
   * no_telepo/hp
   * keterangan
   * pekerjaan
   * tgl_gabung
   * status_pemilik
   ```
   ```bash
    DROP TABLE IF EXISTS `daftar_pemilik`;
    CREATE TABLE `daftar_pemilik` (
     `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
     `no_plat` varchar(50) NOT NULL DEFAULT '',
     `no_ktp` varchar(50) NOT NULL DEFAULT '',
     `nama` varchar(50) NOT NULL DEFAULT '',
     `alamat` varchar(50) NOT NULL DEFAULT '',
     `no_telp_hp` varchar(50) NOT NULL DEFAULT '',
     `pekerjaan` varchar(50) NOT NULL DEFAULT '',
     `tgl_gabung` varchar(50) NOT NULL DEFAULT '',
     `status_pemilik` varchar(50) NOT NULL DEFAULT '',
     `ketreangan` varchar(50) NOT NULL DEFAULT '',
     PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ```
3. Daftar Mobil
   ```bash
   * no_pintu
   * no_plat
   * no_rangka/mesin
   * nama_pemilik
   * no_ktp_pemilik
   * status_mobil
   * tgl_gabung
   * keterangan
   * status_mobil
   ```

   ```bash
   DROP TABLE IF EXISTS `daftar_mobil`;
   CREATE TABLE `daftar_mobil` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `no_pintu` varchar(50) NOT NULL DEFAULT '',
    `no_plat` varchar(50) NOT NULL DEFAULT '',
    `no_rangka_mesin` varchar(50) NOT NULL DEFAULT '',
    `no_ktp` varchar(50) NOT NULL DEFAULT '',
    `nama_pemilik` varchar(50) NOT NULL DEFAULT '',
    `tgl_gabung` varchar(50) NOT NULL DEFAULT '',
    `status_mobil` varchar(50) NOT NULL DEFAULT '',
    `ketreangan` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```
4. Daftar Supir
   ```bash
   * no_ktp_supir
   * nama_supir
   * alamat
   * no_hp
   * no_plat/no_polisi
   * status_supir
   ```
   ```bash
   DROP TABLE IF EXISTS `daftar_supir`;
   CREATE TABLE `daftar_supir` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `no_ktp_supir` varchar(50) NOT NULL DEFAULT '',
    `nama_supir` varchar(50) NOT NULL DEFAULT '',
    `alamat` varchar(50) NOT NULL DEFAULT '',
    `no_hp` varchar(50) NOT NULL DEFAULT '',
    `no_Plat` varchar(50) NOT NULL DEFAULT '',
    `status_supir` varchar(50) NOT NULL DEFAULT '',
    `ketreangan` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```
5. Angsuran
   ```bash
   * no_plat
   * total_jumlah_angsuran
   * tenor
   * jumlah_angsuran_perbulan
   * tgl_jatuh_tempo_pertama
   * tgl_create_data
   * tgl_update
   * status_angsuran
   ```
   ```bash
   DROP TABLE IF EXISTS `angsuran`;
   CREATE TABLE `angsuran` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `no_plat` varchar(50) NOT NULL DEFAULT '',
    `total_jumlah_angsuran` varchar(50) NOT NULL DEFAULT '',
    `tenor` varchar(50) NOT NULL DEFAULT '',
    `jumlah_angsuran_perbulan` varchar(50) NOT NULL DEFAULT '',
    `tgl_jatuh_tempo_pertama` varchar(50) NOT NULL DEFAULT '',
    `tgl_create_data` varchar(50) NOT NULL DEFAULT '',
    `tgl_update` varchar(50) NOT NULL DEFAULT '',
    `status_angsuran` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```

   Detail Angsuran :
   ```bash
   * id_angsuran
   * tgl_bayar
   * jumlah_bayar
   * angsuran_ke
   * jumlah_hari_denda
   * total_jumlah_denda
   * persen_bunga_denda
   * tgl_create_data
   * tgl_update
   * status_angsuran_bulanan
   ```

   ```bash
   DROP TABLE IF EXISTS `detail_angsuran`;
   CREATE TABLE `detail_angsuran` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `no_plat` varchar(50) NOT NULL DEFAULT '',
    `total_jumlah_angsuran` varchar(50) NOT NULL DEFAULT '',
    `tenor` varchar(50) NOT NULL DEFAULT '',
    `jumlah_angsuran_perbulan` varchar(50) NOT NULL DEFAULT '',
    `tgl_jatuh_tempo_pertama` varchar(50) NOT NULL DEFAULT '',
    `tgl_create_data` varchar(50) NOT NULL DEFAULT '',
    `tgl_update` varchar(50) NOT NULL DEFAULT '',
    `status_angsuran` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```
6. Iuran
   > Iuran Bayar Dimuka Dapat Potongan 5 Hari.

   ```bash
   * tgl_bayar
   * jumlah_bayar
   * tgl_iuran
   * tgl_create_data
   * tgl_update
   * status_iuran
   * keterangan
   ```
   ```bash
   DROP TABLE IF EXISTS `iuran`;
   CREATE TABLE `iuran` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `no_plat` varchar(50) NOT NULL DEFAULT '',
    `tgl_bayar` varchar(50) NOT NULL DEFAULT '',
    `tgl_iuran` varchar(50) NOT NULL DEFAULT '',
    `tgl_create_data` varchar(50) NOT NULL DEFAULT '',
    `tgl_update` varchar(50) NOT NULL DEFAULT '',
    `status_iuran` varchar(50) NOT NULL DEFAULT '',
    `keterangan` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```
7. Daftar Biaya
   ```bash
   * kode_biaya
   * nama_biaya
   * kategori
   * jumlah_biaya
   * tgl_create_data
   * tgl_update
   * status_biaya
   * keterangan
   ```
   ```bash
   DROP TABLE IF EXISTS `daftar_biaya`;
   CREATE TABLE `daftar_biaya` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `kode_biaya` varchar(50) NOT NULL DEFAULT '',
    `nama_biaya` varchar(50) NOT NULL DEFAULT '',
    `kategori_id` varchar(50) NOT NULL DEFAULT '',
    `jumlah_biaya` varchar(50) NOT NULL DEFAULT '',
    `tgl_create_data` varchar(50) NOT NULL DEFAULT '',
    `tgl_update` varchar(50) NOT NULL DEFAULT '',
    `status_biaya` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```
8. Kategori Biaya
   1. STNK
   2. Pajak
   3. Denda
   4. Bunga
   5. Perawatan
   6. Adm Lain
   ```bash
   DROP TABLE IF EXISTS `kategori_biaya`;
   CREATE TABLE `kategori_biaya` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```
9. Perawatan
   ```bash
   no_Plat
   nama_perawatan
   kategori_perawatan
   tgl_perawatan
   tgl_perawatan_berikutnya
   status_perawatan
   biaya
   keterangan
   supir
   ```
   ```bash
   DROP TABLE IF EXISTS `perawatan`;
   CREATE TABLE `perawatan` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `no_plat` varchar(50) NOT NULL DEFAULT '',
    `nama_perawatan` varchar(50) NOT NULL DEFAULT '',
    `tgl_perawatan` varchar(50) NOT NULL DEFAULT '',
    `tgl_perawatan_berikutnya` varchar(50) NOT NULL DEFAULT '',
    `biaya` varchar(50) NOT NULL DEFAULT '',
    `supir_id` varchar(50) NOT NULL DEFAULT '',
    `keterangan` varchar(50) NOT NULL DEFAULT '',
    `status_perawatan` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```
   kategori perawatan
   ```bash
   DROP TABLE IF EXISTS `kategori_perawatan`;
   CREATE TABLE `kategori_perawatan` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL DEFAULT '',
    PRIMARY KEY (`id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
   ```

   <!-- Status -->
   1. hapus= 0, aktif/ada=1,
   2. angsuran => lunas=3 , menunggak=2
   3. status_urusan=> proses=2, selesai=3
   4. iuran=> 2 tdk bayar, 3 tdk dengan keterangan, 4 bayar
