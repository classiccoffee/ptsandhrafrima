<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Stnk_api extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('stnk_model');
        $this->load->helper('my_api');
	}

    public function search_get($no_plat)
	{
		if(!is_null($no_plat) || !empty($no_plat))
        {
            $query = $this->stnk_model->get_by(array('no_plat' => $no_plat));
            if($query)
            {
                $tmp = true;
                foreach ($query as $value)
                {
                    if($value['status_stnk'] == 1)
                        $tmp = false;
                }

                if($tmp == true)
                    $this->response($this->set_message_success($query, 'Your Url Here', '', true), 200);
                else
                    $this->response($this->set_message_success('error', 'Your Url Here', 'Data Tidak Ditemukan', false), 200);
            }
    		else
                $this->response($this->set_message_success('error', 'Your Url Here', 'Nomo Plat '.$no_plat.' tidak terdaftar', false), 404);
        }
        else
        {
            $query = $this->stnk_model->get_all();
            if($query)
    		      $this->response($this->set_message_success($query, 'Your Url Here', '', true), 200);
    		else
                $this->response($this->set_message_success('error', 'Your Url Here', 'Data tabel stnk kosong. Jumlah Record 0', false), 404);
        }
	}

	public function find_get()
	{
		$data = array('no_plat' => $this->get('no_plat'));

		$result = $this->stnk_model->get_by($data);

		if(!is_null($result))
			$this->response($this->set_message_success($result, 'Your Url Here', 'Nomor Plat '.$data->no_plat.' ditemukan', true), 200);
		else
			$this->response($this->set_message_success('error', 'Your Url Here', 'Nomor plat '.$data->no_plat.' tidak ditemukan', false), 404);
	}

    public function save_post()
    {
        $stnk_data = array(
                        'no_plat'           => $this->input->post('no_plat'),
                        'nama_pemilik'      => $this->input->post('nama_pemilik'),
                        'alamat'            => $this->input->post('alamat'),
                        'merk'              => $this->input->post('merek'),
                        'type'              => $this->input->post('type'),
                        'jenis'             => $this->input->post('jenis'),
                        'model'             => $this->input->post('model'),
                        'tahun_pembuatan'   => $this->input->post('tahun_pembuatan'),
                        'isi_silinder'      => $this->input->post('isi_silinder'),
                        'no_rangka'         => $this->input->post('no_rangka'),
                        'no_mesin'          => $this->input->post('no_mesin'),
                        'warna'             => $this->input->post('warna'),
                        'bahan_bakar'       => $this->input->post('bahan_bakar'),
                        'warna_plat'        => $this->input->post('warna_plat'),
                        'tahun_registrasi'  => $this->input->post('tahun_registrasi'),
                        "no_bpkb"           => $this->input->post('no_bpkb'),
                        "no_lokasi"         => $this->input->post('no_lokasi'),
                        "no_urut_daftar"    => $this->input->post('no_urut_daftar'),
                        "masa_berlaku"      => $this->input->post('masa_berlaku'),
                        "status_stnk"       => $this->input->post('status_stnk')
                    );

        $result = $this->stnk_model->insert($data);

        if($result)
            $this->response($this->set_message_success($result, 'tempat url', 'Insert Success', true), 200);
        else
            $this->response($this->set_message_success(null, 'tempat url', 'Error Insert', false), 404);
    }

    public function stnk_delete()
    {

    }
}

/* End of file sign_api.php */
/* Location: ./application/controllers/api/sign_api.php */
