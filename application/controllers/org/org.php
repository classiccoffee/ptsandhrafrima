<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Org extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array(
					'_content' 		=> 'org/_content_table',
					'_head_title' 	=> 'Page Org'
				);


		$this->load->view('_head', 	 $data);
		$this->load->view('_body',   $data);
		$this->load->view('_footer', $data);
	}

}
