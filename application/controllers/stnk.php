<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Stnk extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('stnk_model');
	}

    public function index()
    {
        $data = array('_content' => 'stnk/index', '_subtitle' => 'Daftar STNK');

        $this->load->view('_head',$data);
        $this->load->view('_body',$data);
        $this->load->view('_footer');
    }

	public function urusan_stnk(){
        $data = array('_content' => 'stnk/form');
        $this->load->view('_head',$data);
        $this->load->view('_body',$data);
        $this->load->view('_footer',$data);
    }

    public function urusan_pajak()
    {
    }

    public function tambah_stnk_baru()
    {
        $data = array('_content' => 'stnk/form_stnk');
        $this->load->view('_head',$data);
        $this->load->view('_body',$data);
        $this->load->view('_footer');
    }

    public function ajax_tables()
	{
		$list = $this->stnk_model->_get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $val) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $val->no_plat;
			$row[] = $val->nama_pemilik;
			$row[] = $val->alamat;
			$row[] = $val->merk;
			$row[] = $val->type;
			$row[] = $val->jenis;
			$row[] = $val->model;
			$row[] = $val->tahun_pembuatan;
            $row[] = $val->isi_silinder;
            $row[] = $val->no_rangka;
  	 		$row[] = $val->no_mesin;
            $row[] = $val->warna;
      	    $row[] = $val->bahan_bakar;
			$row[] = $val->warna_plat;
			$row[] = $val->tahun_registrasi;
			$row[] = $val->no_bpkb;
			$row[] = $val->no_lokasi;
			$row[] = $val->no_urut_daftar;
			$row[] = $val->masa_berlaku;
            $row[] = $val->status_stnk;
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$val->no_plat."'".')"><i class="fa fa-pencil-square-o"></i></a> <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$val->no_plat."'".')"><i class="fa fa-trash-o"></a>';




			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->stnk_model->_count_all(),
						"recordsFiltered" => $this->stnk_model->_count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

}
