<!doctype html>
<html lang="en" class="fullscreen-bg">
  <head>
  	<title>Login | Klorofil - Free Bootstrap Dashboard Template</title>
  	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  	<!-- CSS -->
  	<link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>">
  	<link rel="stylesheet" href="<?php echo site_url('assets/css/vendor/icon-sets.css');?>">
  	<link rel="stylesheet" href="<?php echo site_url('assets/css/main.min.css');?>">
  	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
  	<link rel="stylesheet" href="<?php echo site_url('assets/css/demo.css');?>">
  	<!-- GOOGLE FONTS -->
  	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
  	<!-- ICONS -->
  	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url('assets/img/apple-icon.png');?>">
  	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url('assets/img/favicon.png');?>">
  </head>
  <body>
    <div id="app">
      <!-- WRAPPER -->
    	<div id="wrapper">
    		<div class="vertical-align-wrap">
    			<div class="vertical-align-middle">
    				<div class="auth-box ">
    					<div class="left">
    						<div class="content">
    							<!-- <div class="logo text-center"><img src="assets/img/logo-dark.png" alt="Klorofil Logo"></div> -->
    							<div class="logo text-center"><h2>PT Sandhrafrima</h2></div>
    							<form class="form-auth-small" action="index.html">
    								<div class="form-group">
    									<label for="signup-email" class="control-label sr-only">Email</label>
    									<input type="email" class="form-control input-lg" id="signup-email" value="admin@domain.com" placeholder="Email">
    								</div>
    								<div class="form-group">
    									<label for="signup-password" class="control-label sr-only">Password</label>
    									<input type="password" class="form-control input-lg" id="signup-password" value="thisisthepassword" placeholder="Password">
    								</div>
    								<div class="form-group clearfix">
    									<label class="fancy-checkbox element-left">
    										<input type="checkbox">
    										<span>Remember me</span>
    									</label>
    								</div>
    								<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
    								<div class="bottom">
    									<span><i class="fa fa-lock"></i> <a href="#">Forgot password?</a></span>
    								</div>
    							</form>
    						</div>
    					</div>
    					<div class="right">
    						<div class="overlay"></div>
    						<div class="content text">
    							<h1 class="heading">Admin Dashboard PT San Dhra Frima</h1>
    							<p>by The Develovers</p>
    						</div>
    					</div>
    					<div class="clearfix"></div>
    				</div>
    			</div>
    		</div>
    	</div>
    	<!-- END WRAPPER -->
    </div>
    <script src="<?php echo site_url('vue/vue.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo site_url('vue/vue-router.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo site_url('vue/axios.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo site_url('vue/vee-validate.js'); ?>" type="text/javascript"></script>
    <script>
        Vue.use(VeeValidate); // good to go.
    </script>
    <script src="<?php echo site_url('vue/app.js'); ?>" type="text/javascript"></script>
  </body>
</html>
