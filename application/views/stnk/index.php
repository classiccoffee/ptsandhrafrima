<div class="custom-tabs-line tabs-line-bottom left-aligned">
    <ul class="nav" role="tablist">
        <li class="active">
            <a href="#tab-bottom-left1" role="tab" data-toggle="tab" aria-expanded="false">STNK</a>
        </li>
        <li class=""><a href="#tab-bottom-left2" role="tab" data-toggle="tab" aria-expanded="true">Urusan STNK </a></li>
        <li class=""><a href="#tab-bottom-left3" role="tab" data-toggle="tab" aria-expanded="true">Urusan Pajak </a></li>
    </ul>
</div>
<div class="tab-content">
  <div class="tab-pane fade in active" id="tab-bottom-left1">
    <div class="row">
      <div class="col-md-12">
          <table id="table" class="table table-striped" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>No Plat</th>
                      <th>Nama Pemilik</th>
                      <th>Alamat</th>
                      <th>Merk</th>
                      <th>Type</th>
                      <th>Jenis</th>
                      <th>Model</th>
                      <th>Tahun Pembuatan</th>
                      <th>Isi Silinder</th>
                      <th>No Rangka</th>
                      <th>No Mesin</th>
                      <th>Warna</th>
                      <th>Bahan Bakar</th>
                      <th>Warna Plat</th>
                      <th>Tahun Registrasi</th>
                      <th>No BPKP</th>
                      <th>No Lokasi</th>
                      <th>No Urut Daftar</th>
                      <th>Masa Berlaku</th>
                      <th>Status STNK</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="tab-bottom-left2">
    <div class="row">
      <div class="col-md-12">

      </div>
    </div>
  </div>
</div>
