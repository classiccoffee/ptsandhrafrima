
<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<?php $this->load->view('_side_menu.php'); ?>
		<!-- END SIDEBAR -->


		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<?php $this->load->view('_nav_menu.php'); ?>
			<!-- <pemilikbarunav></pemilikbarunav> -->

			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content" style="margin:-8px !important;">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<!-- <test></test> -->
						</div>
						<div class="panel-body">
							<!-- Content -->
							  <?php $this->load->view($_content); ?>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; 2016</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
