<!doctype html>
<html lang="en">

<head>
	<title>Dashboard | Klorofil - Free Bootstrap Dashboard Template</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?php echo site_url('assets/css/vendor/icon-sets.css');?>">
	<link rel="stylesheet" href="<?php echo site_url('assets/css/main.min.css');?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo site_url('assets/css/custom.css');?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url('assets/img/apple-icon.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url('assets/img/favicon.png');?>">
</head>

<body>
	<script>
		var myurl='test';
		console.log(myurl);
	</script>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<vsidebar></vsidebar>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<vnavbar></vnavbar>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!--
					<div class="custom-tabs-line tabs-line-bottom left-aligned">
						<ul class="nav" role="tablist">
						<li class=""><a href="#tab-bottom-left1" role="tab" data-toggle="tab" aria-expanded="false">Recent Activity</a></li>
						<li class="active"><a href="#tab-bottom-left2" role="tab" data-toggle="tab" aria-expanded="true">Projects <span class="badge">7</span></a></li>
						</ul>
					</div> -->
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<!-- <test></test> -->
							<h3 class="panel-title">Tampilan Mingguan</h3>
							<p class="panel-subtitle">Period: Oct 14, 2016 - Oct 21, 2016</p>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="metric">
										<span class="icon"><i class="fa fa-download"></i></span>
										<p>
											<span class="number">1,252</span>
											<span class="title">Jumlah Mobil</span>
										</p>
									</div>
								</div>
								<div class="col-md-6">
									<div class="metric">
										<span class="icon"><i class="fa fa-shopping-bag"></i></span>
										<p>
											<span class="number">203</span>
											<span class="title">Total Iuran Minggu Ini</span>
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<h3 class="panel-title">Daftar Mobil</h3>
									<br />
									<table class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>#</th>
												<th>First Name</th>
												<th>Last Name</th>
												<th>Username</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>Steve</td>
												<td>Jobs</td>
												<td>@steve</td>
											</tr>
											<tr>
												<td>2</td>
												<td>Simon</td>
												<td>Philips</td>
												<td>@simon</td>
											</tr>
											<tr>
												<td>3</td>
												<td>Jane</td>
												<td>Doe</td>
												<td>@jane</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; 2016</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="<?php echo site_url('assets/js/jquery/jquery-2.1.0.min.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/bootstrap/bootstrap.min.js');?>"></script>
	<script src="<?php echo site_url('assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js');?>"></script>
	<script src="<?php echo site_url('assets/js/klorofil.min.js');?>"></script>
	<script src="<?php echo site_url('vue/vue.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo site_url('vue/vue-router.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo site_url('vue/axios.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo site_url('vue/vee-validate.js'); ?>" type="text/javascript"></script>
	<script>
			Vue.use(VeeValidate); // good to go.
	</script>
	<script src="<?php echo site_url('vue/app.js'); ?>" type="text/javascript"></script>

</body>

</html>
