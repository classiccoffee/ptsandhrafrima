<!doctype html>
<html lang="en">

<head>
	<title>Dashboard | Klorofil - Free Bootstrap Dashboard Template</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<!-- CSS -->

	<link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.min.css');?>">
	<link href="<?php echo site_url('assets/css/jquery.dataTables.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('assets/css/responsive.dataTables.min.css'); ?>" rel="stylesheet">
	<link href="<?php //echo site_url('assets/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo site_url('assets/css/vendor/icon-sets.css');?>">
	<link rel="stylesheet" href="<?php echo site_url('assets/css/main.css');?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo site_url('assets/css/demo.css');?>">
	<link href="<?php //echo site_url('assets/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">

	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url('assets/img/apple-icon.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url('assets/img/favicon.png');?>">
</head>
