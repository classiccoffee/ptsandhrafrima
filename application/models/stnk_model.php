<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stnk_model extends MY_Model
{
    public $_table;
    public $primary_key = 'id';

    public $before_create = array('create_time');
    public $before_update = array('update_time');

    public $column_order = array(null, 'no_plat', 'nama_pemilik', 'alamat', 'merk', 'type',
                                'jenis', 'model', 'tahun_pembuatan', 'isi_silinder', 'no_rangka',
                                'no_mesin', 'warna', 'status_stnk');
    public $column_search = array('no_plat', 'nama_pemilik', 'alamat', 'merk', 'type', 'jenis',
                                  'model','tahun_pembuatan','status_stnk');
    public $order = array('id' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('names_table');
        $this->_table = $this->names_table->dto_stnk;
    }

    protected function create_time($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s');
        return $book;
    }

    protected function update_time($data)
    {
        $data['updated_at'] = date('Y-m-d H:i:s');
        return $book;
    }

    public function _get_field()
    {
        $result = $this->db->list_fields($this->_table);
        foreach($result as $field)
        {
            $data[] = $field;

        }
         return $data;
    }

    private function _get_datatable_query()
    {
        $this->db->from($this->_table);

        $index = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($index === 0) // first loop
                {
                    $this->db->group_start();
                    // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $index)
                {
                    //last loop
                    $this->db->group_end(); //close bracket
                }

            }

            $index++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function _get_datatables()
    {
        $this->_get_datatable_query();

        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();

        return $query->result();
    }

    function _count_filtered()
    {
        $this->_get_datatable_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function _count_all()
    {
        $this->db->from($this->_table);
        return $this->db->count_all_results();
    }


}
